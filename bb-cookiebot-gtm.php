<?php
/*
Plugin Name: Cookiebot & Google Tag Manager
Description: Add Cookiebot and Google Tag Manager to the website
Version: 1.0
Author: BRANDON & BRANDA
*/

add_action('wp_head', function () {
	if (defined('GTM_CODE')) {
        echo '<script data-cookieconsent="ignore">
            window.dataLayer = window.dataLayer || [];

            function gtag() {
                dataLayer.push(arguments);
            }

            gtag("consent", "default", {
                ad_personalization: "denied",
                ad_storage: "denied",
                ad_user_data: "denied",
                analytics_storage: "denied",
                functionality_storage: "denied",
                personalization_storage: "denied",
                security_storage: "granted",
                wait_for_update: 500,
            });

            gtag("set", "ads_data_redaction", true);
        </script>';

        echo '<!-- Google Tag Manager -->
            <script data-cookieconsent="ignore">(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({\'gtm.start\':
            new Date().getTime(),event:\'gtm.js\'});var f=d.getElementsByTagName(s)[0],
            j=d.createElement(s),dl=l!=\'dataLayer\'?\'&l=\'+l:\'\';j.async=true;j.src=
            \'https://www.googletagmanager.com/gtm.js?id=\'+i+dl;f.parentNode.insertBefore(j,f);
            })(window,document,\'script\',\'dataLayer\', \'' . GTM_CODE . '\');</script>
            <!-- End Google Tag Manager -->';
	}
    
    if (defined('COOKIEBOT_CBID')) {
        echo '<script id="Cookiebot" src=https://consent.cookiebot.com/uc.js data-cbid="' . COOKIEBOT_CBID . '" data-blockingmode="auto" type="text/javascript"></script>';
    }
}, 0);
